-- mcl_decor/misc.lua

local S = minetest.get_translator(minetest.get_current_modname())

--- Coalquartz Tile ---
minetest.register_node("mcl_decor:coalquartz_tile", {
	description = S("Coalquartz Tile"),
	tiles = {"mcl_decor_coalquartz_tile.png"},
	is_ground_content = false,
	groups = {pickaxey=1, flammable=1, quartz_block=1, building_block=1, material_stone=1, fire_encouragement=5, fire_flammability=5},
	sounds = mcl_sounds.node_sound_stone_defaults(),
	_mcl_blast_resistance = 4,
	_mcl_hardness = 3,
})
minetest.register_craft({
	output = "mcl_decor:coalquartz_tile",
	recipe = {
		{"mcl_core:coal_lump", "mcl_nether:quartz"},
		{"mcl_nether:quartz", "mcl_core:coal_lump"}
	}
})
minetest.register_craft({
	output = "mcl_decor:coalquartz_tile",
	recipe = {
		{"mcl_nether:quartz", "mcl_core:coal_lump"},
		{"mcl_core:coal_lump", "mcl_nether:quartz"}
	}
})
mcl_stairs.register_stair_and_slab_simple("coalquartz_tile", "mcl_decor:coalquartz_tile", S("Coalquartz Stair"), S("Coalquartz Slab"), S("Double Coalquartz Slab"))



--- Table Lamp ---
minetest.register_node("mcl_decor:table_lamp", {
	description = S("Table Lamp"),
	tiles = {"mcl_decor_table_lamp.png"},
	use_texture_alpha = "clip",
	drawtype = "mesh",
	mesh = "mcl_decor_table_lamp.obj",
	paramtype = "light",
	stack_max = 64,
	selection_box = {
		type = "fixed",
		fixed = {-0.3125, -0.5, -0.3125, 0.3125, 0.5, 0.3125},
	},
	collision_box = {
		type = "fixed",
		fixed = {-0.3125, -0.5, -0.3125, 0.3125, 0.5, 0.3125},
	},
	is_ground_content = false,
	light_source = minetest.LIGHT_MAX,
	groups = {handy=1, axey=1, attached_node=1, deco_block=1, flammable=-1},
	sounds = mcl_sounds.node_sound_wood_defaults(),
	_mcl_blast_resistance = 0.9,
	_mcl_hardness = 0.9,
})
minetest.register_craft({
	output = "mcl_decor:table_lamp 3",
	recipe = {
		{"group:wool", "group:wool", "group:wool"},
		{"group:wool", "mcl_torches:torch", "group:wool"},
		{"mcl_core:cobble", "mcl_core:cobble", "mcl_core:cobble"}
	}
})
