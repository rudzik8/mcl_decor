-- mcl_decor/hedges.lua

local S = minetest.get_translator(minetest.get_current_modname())

-- API
function mcl_decor.register_hedge(name, desc, material, tiles)
	-- use mcl_fences api to register hedge
	mcl_fences.register_fence(
		name .. "_hedge",
		desc,
		tiles,
		{handy = 1, axey = 1, hedge = 1, deco_block = 1, flammable = 2, fire_encouragement = 10, fire_flammability = 10},
		1, 1,
		{"group:hedge"},
		mcl_sounds.node_sound_leaves_defaults()
	)
	-- crafting recipe
	minetest.register_craft({
		output = "mcl_decor:" .. name .. "_hedge" .. " 6",
		recipe = {
			{material, "mcl_core:stick", material},
			{material, "mcl_core:stick", material},
		}
	})
end



-- REGISTER
mcl_decor.register_hedge("oak", S("Oak Hedge"), "mcl_core:leaves", "default_leaves.png")
mcl_decor.register_hedge("dark", S("Dark Oak Hedge"), "mcl_core:darkleaves", "mcl_core_leaves_big_oak.png")
mcl_decor.register_hedge("jungle", S("Jungle Hedge"), "mcl_core:jungleleaves", "default_jungleleaves.png")
mcl_decor.register_hedge("acacia", S("Acacia Hedge"), "mcl_core:acacialeaves", "default_acacia_leaves.png")
mcl_decor.register_hedge("spruce", S("Spruce Hedge"), "mcl_core:spruceleaves", "mcl_core_leaves_spruce.png")
mcl_decor.register_hedge("birch", S("Birch Hedge"), "mcl_core:birchleaves", "mcl_core_leaves_birch.png")
mcl_decor.register_hedge("mangrove", S("Mangrove Hedge"), "mcl_mangrove:mangroveleaves", "mcl_mangrove_leaves.png")

-- all hedges should be fuel
minetest.register_craft({
	type = "fuel",
	recipe = "group:hedge",
	burntime = 5,
})
